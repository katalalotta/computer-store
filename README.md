# Frontend assignment 1: Computer store with vanilla JS

## Table of contents

- Background
- Installation
- Usage
- Contributors


### Background

This dynamic web page represents a computer store. The application was made with HTML5, Bootsrap and vanilla JavaScript. It has three different sections.

1. The Bank – an area where you will store funds and make bank loans
2. Work – an area to increase your earnings and deposit cash into your bank balance
3. Laptops – an area to select and display information about the merchandise

The styling and layot of the UI is not fully finished. 

### Installation

1. Clone this repository with HTTPS
2. Open project in IDE
3. Run with browser

### Contributors

This exercise was created by Noroff School of Technology and this solution was implemented by Lotta Lampola.







