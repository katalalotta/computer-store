if (typeof document === 'undefined') {
    // during server evaluation
    console.log("hep")
} else {
    console.log("browser")
}
//JS classes
class Bank{
    constructor(){
        this.balance=200;
        this.loan=0;
    }

    setBalance(b){
        this.balance=Number(b);
    }

    getBalance(){
        return this.balance;
    }

    addBalance(b){
        this.balance+=Number(b);
    }

    reduceBalance(b){
        this.balance-=Number(b);
    }

    setLoan(l){
        this.loan=Number(l);
    }

    getLoan(){
        return this.loan;
    }

    applyLoan(l){
        if (this.loan > 0) {
            return "You cannot have two loans at once, please pay back the first one"
        } else if (l > 2*this.balance){
            return "You cannot get a loan more than double of your balance"
        } else {
            this.setLoan(l);
            this.addBalance(l);
            return true;
        }

    }

    transfer(amount){
        if (this.loan > 0){
            this.loan = this.loan - amount*0.1;
            this.balance += amount*0.9;
        } else {
            this.balance+=amount;
        }
        
    }
}

class Work{
    constructor(bank){
        this.bank = bank;
        this.salary = 0;
    }

    getSalary(){
        return this.salary;
    }

    work(){
        this.salary+=100;
    }

    transferToBank(){
        this.bank.transfer(this.salary);
        this.salary=0;
    }


}
const bank = new Bank();
const work = new Work(bank);

const refreshBalance = () => {
    bankBalanceEl.innerText = bank.getBalance();
}

const refreshLoan = () => {
    bankLoanEl.innerText = bank.getLoan();
    console.log("Loan amount when refresh: "+bank.getLoan())
}

const refreshSalary = () => {
    salaryEl.innerText = work.getSalary();
}
//Bank
const bankBalanceEl = document.getElementById("bankBalance");
refreshBalance();
const getLoanButtonEl = document.getElementById("getLoan");
const bankSection = document.getElementById("loanDiv");
const loanEl = document.getElementById("loanSpan")
const bankLoanEl = document.getElementById("loanBalance")
//Work
const salaryEl = document.getElementById("salary");
salaryEl.innerText = work.getSalary();
const putInBankButtonEl = document.getElementById("putInBank");
const workButtonEl = document.getElementById("work");
//Selection
const laptopSelectorEl = document.getElementById("laptopSelector");
const laptopFeaturesEl = document.getElementById("laptopFeatures");
//Info
const laptopNameEl = document.getElementById("laptopName");
const laptopImage = document.getElementById("image");
const laptopDescriptionEl = document.getElementById("laptopDescription");
const laptopPriceEl = document.getElementById("laptopPrice");
const buyButtonEl = document.getElementById("buy");

let computers = [];

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(drinks => addComputersToStore(computers))

const addComputersToStore = (computers) => {
    computers.forEach(c => addComputerToStore(c))
}

const addComputerToStore = computer => {
    const computerEl = document.createElement("option")
    computerEl.value = computer.id;
    computerEl.appendChild(document.createTextNode(computer.title))
    laptopSelectorEl.appendChild(computerEl)
    //Inital values to selector and info
    laptopFeaturesEl.innerText = computers[0].specs;
    updateInfoSection(computers[0])
}


const handleLaptopSelectorChange = e => {
    const selectedLaptop = computers[e.target.selectedIndex];
    laptopFeaturesEl.innerText = selectedLaptop.specs;
    updateInfoSection(selectedLaptop);
}

const updateInfoSection = selectedLaptop => {
    laptopNameEl.innerText = selectedLaptop.title;
    laptopDescriptionEl.innerText = selectedLaptop.description;
    laptopPriceEl.innerText = selectedLaptop.price+" NOK";
    laptopImage.src = "https://noroff-komputer-store-api.herokuapp.com/"+selectedLaptop.image;
}

const addLoanToUI = () => {
    loanEl.innerText = "Loan";
    bankLoanEl.innerText = bank.getLoan();
    bankSection.appendChild(loanEl)
    bankSection.appendChild(bankLoanEl)
}

const handleGetLoanClick = e => {
    console.log("Get loan-button clicked!")
    let amount;
    while(isNaN(amount)){
        amount = prompt("Enter amount of loan applied:", "")
        if (isNaN(amount)){
            alert("Please enter digits only!")
        }
    }

    let result = bank.applyLoan(amount);
    if (result != true){
        alert(result)
    } else {
        alert("Your application for loan of "+amount+" has been granted!")
        console.log("Loan: "+bank.getLoan())
        console.log("Balance: "+bank.getBalance())
        refreshBalance();
        addLoanToUI();
    }
}

const handleWorkClick = e => {
    console.log("Work button clicked")
    work.work();
    refreshSalary();

}

const handlePutInBankClick = e => {
    console.log("Put in bank button clicked")
    work.transferToBank()
    refreshBalance();
    refreshSalary();
    if (bank.getLoan > 0){
    refreshLoan();
    }
}

const handleBuyLaptopClick = e => {
    console.log("Buy button clicked")
    let laptopId = laptopSelectorEl.value;
    tryToBuyLapTop(laptopId);

}

const tryToBuyLapTop = (id) => {
    let price = computers[id-1].price;
    console.log(price)
    let balance = bank.getBalance();
    if (balance < price) {
        alert("Not enough balance to buy the laptop!")
    } else {
        bank.reduceBalance(price);
        alert("You are now owner of a new laptop!")
        refreshBalance();
    }
}



laptopSelectorEl.addEventListener('change',handleLaptopSelectorChange)
getLoanButtonEl.addEventListener('click',handleGetLoanClick)
workButtonEl.addEventListener('click',handleWorkClick)
putInBankButtonEl.addEventListener('click', handlePutInBankClick)
buyButtonEl.addEventListener('click',handleBuyLaptopClick)






